import { add } from './index';

describe( "string calculator", () => {
    test( "should return 0 with an empty string as input", () => {
        let actual = add( '' );
        expect( actual ).toEqual( 0 );
    } );

    test( "should return sum of unknown amount of numbers", () => {
        let actual = add( '1,2,3,4,5,6' );
        expect( actual ).toEqual( 21 );
    } );
    test( "should return correct sum when numbers are on new lines", () => {
        let actual = add( '1\n2,3' );
        expect( actual ).toEqual( 6 );
    } );
    test( "should return correct sum with any type of delimiters", () => {
        let actual = add( '//;\\n1;2' );
        expect( actual ).toEqual( 3 );
    } );
    test( "should return negatives not allowed when negative number if passed through", () => {
        let actual = add( '-1,2, -3' );
        expect( actual ).toEqual( 'negatives not allowed -1,-3' );
    } );
    test( "should ignore numbers greater than 1000", () => {
        let actual = add( '1001,2' );
        expect( actual ).toEqual( 2 );
    } );
    test( "should ignore numbers greater than 1000", () => {
        let actual = add( '1001,2,3' );
        expect( actual ).toEqual( 5 );
    } );
    test( "should allow multiple delimiters", () => {
        let actual = add( '//[*][%]\n1*2%3' );
        expect( actual ).toEqual( 6 );
    } );
    test( "should allow multiple delimiters", () => {
        let actual = add( '//[*][%]\n12*22%3' );
        expect( actual ).toEqual( 37 );
    } );
} );
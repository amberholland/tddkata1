export function add( numbers: string ): number | string {
    const negNum = /-[0-9]/g;
    let negResult = numbers.match( negNum );

    const numWithoutSpecialCharacters = numbers.replace( /(\r|\n|\s|\D)/gm, ' ' );
    let numsWithoutSpaces = numWithoutSpecialCharacters.split( ' ' );
    const numsWithoutCommas = numsWithoutSpaces.toString().split( ',' );

    if ( negResult ) {
        return `negatives not allowed ${ negResult }`;
    }
    let sum: number = 0;
    for ( let i = 0; i < numsWithoutCommas.length; i++ ) {
        if ( numsWithoutCommas[ i ] !== '' && ( parseInt( numsWithoutCommas[ i ] ) <= 1000 ) ) {

            sum += parseInt( numsWithoutCommas[ i ] );
        }
    }
    return sum;
}

export * from "./index";